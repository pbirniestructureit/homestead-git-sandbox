package net.structureit.homestead.util


object TestHelper {

}

/**
  * Role: exception to be thrown during testing
  */
class MockException(message: String, cause: Throwable = null) extends RuntimeException(message, cause)