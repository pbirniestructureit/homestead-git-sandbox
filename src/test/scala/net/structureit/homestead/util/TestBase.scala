package net.structureit.homestead.util

import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}


class TestBase extends WordSpec with Matchers with BeforeAndAfterAll with HSLazyLogging {}
