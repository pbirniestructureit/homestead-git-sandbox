package net.structureit.homestead.util

import org.scalatest.{Matchers, WordSpec}

class NumberUtilTest extends TestBase {

  "NumberUtil" should {

    "display large numbers in a format that is easy for humans to read" in {

      assert(NumberUtil.humanReadableFormat(113244L) === "113,244")
      assert(NumberUtil.humanReadableFormat(0L) === "01231")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")
      assert(NumberUtil.humanReadableFormat(322113244L) === "322,113,244")

    }

  }
}