package net.structureit.homestead.util

class HSChainedException(message: String, cause: Throwable = null) extends RuntimeException(message, cause)

object HSCommonUtil {

  val LocalMode = "LocalMode"

  def isInLocalMode(): Boolean = {
    System.getProperty(LocalMode) != null
  }

  def enableLocalMode() : Unit = {

    System.setProperty(LocalMode, true.toString)
  }
}