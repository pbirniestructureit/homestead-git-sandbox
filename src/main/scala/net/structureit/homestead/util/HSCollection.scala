package net.structureit.homestead.util

/**
  * Role: A fixed size list - which ensures that we dont cause a memory leak by storing too many values
  *
  * from https://stackoverflow.com/questions/6918731/maximum-length-for-scala-queue/6920366#6920366
  *
  * @param limit
  * @param out
  * @param in
  * @tparam T
  */
class FixedSizeFifo[T](val limit: Int)
                      ( private val out: List[T], private val in: List[T] )
  extends Traversable[T] {

  override def size = in.size + out.size

  def :+( t: T ) = {
    val (nextOut,nextIn) = if (size == limit) {
      if( out.nonEmpty) {
        ( out.tail, t::in )
      } else {
        ( in.reverse.tail, List(t) )
      }
    } else ( out, t::in )
    new FixedSizeFifo( limit )( nextOut, nextIn )
  }

  private lazy val deq = {
    if( out.isEmpty ) {
      val revIn = in.reverse
      ( revIn.head, new FixedSizeFifo( limit )( revIn.tail, List() ) )
    } else {
      ( out.head, new FixedSizeFifo( limit )( out.tail, in ) )
    }
  }
  override lazy val head = deq._1
  override lazy val tail = deq._2

  def foreach[U]( f: T => U ) = ( out ::: in.reverse ) foreach f

}

object FixedSizeFifo {
  def apply[T]( limit: Int ) = new FixedSizeFifo[T]( limit )(List(),List())
}
