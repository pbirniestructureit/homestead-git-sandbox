package net.structureit.homestead.util

import java.io.{File, FileInputStream}
import java.util.Properties

import com.typesafe.config.Config

object HSConfigUtil {

  def loadProperties(filename: String): java.util.Properties = {
    try {
      val prop = new Properties()
      prop.load(new FileInputStream(filename))
      prop
    } catch {
      case e: Exception =>
        throw new HSChainedException(s"cannot open ${filename}", e)
    }
  }

  def checkMandatoryConfigFile(key : String) : Unit = {

    val filePath = System.getProperty(key)

    if (filePath == null){

      throw new HSChainedException(s"${key} is NOT set, please provide -D${key}=xxx")
    }

    val configFile = new File(filePath)

    if (!configFile.canRead){
      throw new HSChainedException(s"${filePath} set BUT unable to read file")
    }
  }


}

trait HSConfigUtil extends HSLazyLogging {

  def dumpApplicationConfig(config: Config): Unit ={

    val dumpFileName = "./debug-merged-config.conf"
    val configDump = config.root().render()
    HSFileUtil.writeToFile(dumpFileName, configDump)

    localLogger.debug(s"dumped final application config to $dumpFileName")
  }


}

