package net.structureit.homestead.util

import org.slf4j.{Logger, LoggerFactory}

/**
  * Role: logging of events via slf4j
  */
trait HSLazyLogging {

  protected lazy val localLogger : Logger = LoggerFactory.getLogger(getClass.getName)
}