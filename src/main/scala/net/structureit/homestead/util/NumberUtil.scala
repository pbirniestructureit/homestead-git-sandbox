package net.structureit.homestead.util

import java.text.NumberFormat
import java.util.Locale

object NumberUtil {

  val humanReadableNumberFormat = NumberFormat.getNumberInstance(Locale.US)

  def humanReadableFormat(input: Long): String = {

    humanReadableNumberFormat.format(input)
  }
}
