package net.structureit.homestead.util

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import scala.io.Source


object HSFileUtil {

  def writeToFile(fileName: String, contents : String) = {
    val file = new File(fileName)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(contents)
    bw.close()
  }

  def readFromFile(fileName: String): Unit ={
    Source.fromFile(fileName).mkString
  }

  def getTestResource(resourceName: String): File = {

    val path = HSFileUtil.getClass.getResource(resourceName)

    if (path == null){
      throw new HSChainedException(s"Resource |$resourceName| was not found", null)
    }

    val file = new File(path.getPath)

    file
  }
}
