#!/bin/bash
#
#  Role: apply schema evolution to database
#

# determine the dir this script is in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


CHECKOUT_DIR="homestead-infrastructure"

if [ -d ${CHECKOUT_DIR} ]; then
    rm -rf  ${CHECKOUT_DIR}
fi

echo "cloning ${CHECKOUT_DIR}"
git clone  --depth=1 --branch master --single-branch git@bitbucket.org:dealviewtech/homestead-infrastructure.git  ${CHECKOUT_DIR}

./${CHECKOUT_DIR}/databases/progresstracker/migrate.sh local

