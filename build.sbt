import sbt.Keys.libraryDependencies
import sbtassembly.PathList

name := "SparkESCommon"

version := "1.0"

scalaVersion := "2.11.0"

dependencyOverrides ++= Set(
  "commons-net" % "commons-net" % "2.2",
  "com.google.guava" % "guava" % "14.0.1",
  "io.netty" % "netty" % "3.8.0.Final"
)

lazy val syncTask = TaskKey[Unit]("sync-infra")

lazy val root = (project in file(".")).
  enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "net.structureit.homestead.util",
    syncTask:= {

      println("syncTask starting...")

      // export ENV_FILE = "./homestead-infrastructure/env/local.properties"

      "bash sync_infrastructure.sh" !

      println("syncTask completed")
    }
  )


buildInfoOptions += BuildInfoOption.ToMap
buildInfoOptions += BuildInfoOption.ToJson
buildInfoOptions += BuildInfoOption.BuildTime


libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies += "org.apache.commons" % "commons-dbcp2" % "2.0.1"
libraryDependencies += "org.joda" % "joda-convert" % "1.8.1"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.1" % "provided" withSources() withJavadoc()
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.1.1" % "provided" withSources() withJavadoc()
libraryDependencies += "org.apache.spark" %% "spark-hive" % "2.1.1" % "provided" withSources() withJavadoc()



libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.10.1.1"
libraryDependencies += "org.graylog2" % "gelfclient" % "1.4.1"
libraryDependencies += "org.postgresql" % "postgresql" % "9.4.1212"

libraryDependencies += "com.typesafe" % "config" % "1.3.2"

// parse command line args
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0"


// This library is not longer needed as we are using spark 2.1
// for more information see https://www.elastic.co/guide/en/elasticsearch/hadoop/current/spark.html
// believe that "elasticsearch-spark-20" is out of date
//libraryDependencies += "org.elasticsearch" %% "elasticsearch-spark-20" % "5.5.3" withSources() withJavadoc()

// from https://github.com/elastic/elasticsearch-hadoop
// NOTE: "ES-Hadoop 6.x and higher are compatible with Elasticsearch 1.X, 2.X, 5.X, and 6.X"
libraryDependencies += "org.elasticsearch" % "elasticsearch-hadoop" % "6.2.1" withSources() withJavadoc()

libraryDependencies += "com.sksamuel.elastic4s" %% "elastic4s-http" % "6.0.0-rc1"

// TODO  pbirnie why are these here as Overrides
dependencyOverrides ++= Set(
  "com.fasterxml.jackson.core" % "jackson-core" % "2.8.8",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.8",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.8"
)

assemblyMergeStrategy in assembly := {
  // exclude application.conf from the artifact
  case "application.conf"            => MergeStrategy.discard
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

